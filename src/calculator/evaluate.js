'use strict';

function evaluate(str) {
  const trimmedInput = str.trim();
  if (trimmedInput.length === 0) {
    return 0;
  }
  return evaluateNonEmptyInput(trimmedInput);
}

function evaluateNonEmptyInput(trimmedInput) {
  const stack = [];
  for (let part of trimmedInput.split(/\s+/)) {
    if (isOperator(part)) {
      const operationResult = operator(part)(...fetchOperands(stack));
      stack.push(operationResult);
    } else {
      stack.push(parseNumber(part));
    }
  }
  if (stack.length > 1) {
    throw new Error('too many numbers');
  }
  return stack[0];
}

function isOperator(op) {
  return '+-*/'.includes(op);
}

function fetchOperands(stack) {
  const rightOperand = stack.pop();
  const leftOperand = stack.pop();
  if (leftOperand === undefined) {
    throw new Error('missing operand for operator: ' + part);
  }
  return [leftOperand, rightOperand];
}

function operator(op) {
  return function(a, b) {
    switch (op) {
      case '+':
        return a + b;
      case '-':
        return a - b;
      case '*':
        return a * b;
      case '/':
        return a / b;
    }
  };
}

function parseNumber(part) {
  const num = Number(part);
  if (Number.isNaN(num)) {
    throw new Error('Unknown token: ' + part);
  }
  return num;
}

export default evaluate;
